const dig 		= require('gamedig');
const discord 	= require('discord.js'); 

const bot = new discord.Client();

let playerNames = "";

const prefix = '/';
const token = "";

let _getInfo = (ip, port) => {
    return dig.query({
        type: 'minecraft',
        host: ip,
        port: port,
        socketTimeout: 10000,
        udpTimeout: 15000
    });
}

bot.on('message', message => {
	
	if(message.content.indexOf(prefix) !== 0) return;
	
	if(message.author.bot) 
		return;
		
	const args = message.content.slice(prefix.length).trim().split(/ +/g);
	const command = args.shift().toLowerCase();

	switch(command)
	{
		case "server":
		{
			let request = _getInfo('85.10.200.100', 1337);
			request.then(x => {
				playerNames = "";
				
				x.players.forEach((item) => {
					if(item.name == undefined || item.name == "undefined") 
						item.name = "<blank>";
					
					playerNames += `${item.name}\n`;
				});
				
				let info = {
					name: "MangeCraft Vanilla",
					pl: x.raw.numplayers,
					maxpl: x.maxplayers,
					plrs: playerNames
				};
				
				setTimeout(() => {
					message.channel.send(_createEmbed(server[2], true, info.pl, info.maxpl, info.plrs));
				}, 5000);
				
			}).catch(e => {
				console.error(e);
				
				message.channel.send(_createEmbed(server[2], false, 0, 0, "Server is offline")).then(() => console.log("--- Sent Embed ---"));
			});
		}
	}
});

let _createEmbed = (name, online, players, maxplayers, playerNames) => {
	let e = new discord.RichEmbed();
		e.setTitle(name.length > 0 ? name : "Error");
		e.setColor(online ? 0x00FF00 : 0xFF0000);
		e.AddField("Status", online ? "Online" : "Offline");
		e.addField("Players", `Currently ${players}/${maxplayers}`);
		e.setDescription(`\`\`\`${playerNames}\`\`\``);
		e.setTimestamp();
		
	return e;
};

bot.login(token);